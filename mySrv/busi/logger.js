
const pino = require('pino')

const logger = pino(pino.destination({
  sync: false // Asynchronous logging
}))

class Logger {
  get logger() {
    return logger
  }
}

// make it singleton via new
module.exports = new Logger()