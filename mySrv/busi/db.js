const logger = require('./logger').logger

const { Sequelize } = require('sequelize')

class DB {
static initCount = 0

static _sequelize

constructor() {
	DB._sequelize = new Sequelize( {
		dialect: 'sqlite', // 'mariadb',
		storage: 'delme.DB',
		logging: msg => logger.debug(msg),
		define: {
			freezeTableName: true
		}
	})
}

async _init() {
	return new Promise(async (res,rej)=> {
	try {
		logger.info('.')
		if(DB.initCount > 0) return
		DB.initCount++
		await DB._sequelize.authenticate()
		logger.warn('Connection has been established successfully. ', DB.initCount)
		res()
	} catch (error) {
		console.error('Unable to connect to the database:', error)
		rej(error)
	}
	}) /// pro
}//()

get seq() {
	logger.info('get seq')
	return DB._sequelize
}

}// class

module.exports = new DB()