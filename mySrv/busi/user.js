const logger = require('./logger').logger
const { QueryTypes, Model, DataTypes } = require('sequelize')

const DB = require('./db')
const sequelize = DB.seq

class User extends Model {
    static async foo() {
        logger.info('oh hi')
    }
}

User.init({
        pk : { type: DataTypes.UUID},
        name: { type: DataTypes.STRING(30)}
    }, { sequelize,
        indexes: [{ fields: ['pk']}]
})

User.sync({alter:true})

module.exports = sequelize.models.User