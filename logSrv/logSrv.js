
require('dotenv').config()

const fastify = require('fastify')({logger:false})

fastify.route({
    method: 'POST',
    url: '/api/log',
    handler : (req, rep) => {
        console.log(req.body)
        //console.log(req.headers['user-agent'], 'h')
        //console.log(req.ip, 'ip')

        rep
        .code(200)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send({ logger: 'OK' })    
    }// handler
})

fastify.addHook('onError', async (request, reply, error) => {
    console.log(error)
})

const start = async () => {
    try {
        console.log('listening', process.env.PORT)
        await fastify.listen(process.env.PORT)
    } catch (err) {
        fastify.log.error(err)
        console.log(err)
        process.exit(1)
    }
}
start()